package com.devcamp.apilist.allapilist;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AllApiListApplication {

	public static void main(String[] args) {
		SpringApplication.run(AllApiListApplication.class, args);
	}

}
