package com.devcamp.apilist.allapilist.annotation;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValueOfArrayValidator implements ConstraintValidator<ValueOfArray, CharSequence> {
    private List<String> acceptedValues = new ArrayList<String>();

    @Override
    public void initialize(ValueOfArray annotation) {
        String [] list = annotation.arrayClass();
        for (int i = 0; i < list.length; ++i) {
            acceptedValues.add(list[i]);
        }
    }

    @Override
    public boolean isValid(CharSequence value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }

        return acceptedValues.contains(value.toString());
    }
}
