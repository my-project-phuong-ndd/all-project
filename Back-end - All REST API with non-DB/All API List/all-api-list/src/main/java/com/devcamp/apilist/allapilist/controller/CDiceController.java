package com.devcamp.apilist.allapilist.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.devcamp.apilist.allapilist.interfacelist.CThrowErrorInterface;
import com.devcamp.apilist.allapilist.model.CDiceResult;
import com.devcamp.apilist.allapilist.model.CUserDice;
import com.devcamp.apilist.allapilist.model.CVoucher;
import com.devcamp.apilist.allapilist.services.CDiceServices;
import com.devcamp.apilist.allapilist.services.CUserDiceServices;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.github.bucket4j.ConsumptionProbe;


@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/lucky-dice-api")
public class CDiceController implements CThrowErrorInterface{

    @PostMapping("/roll")
    public CDiceResult letRoll (@Valid @RequestBody (required = true) CUserDice userInfo,BindingResult allError) throws Exception {

        if (allError.hasErrors()) {
            Map<String, String> errors = new HashMap <> ();
            allError.getFieldErrors().forEach(
                    error -> errors.put(error.getField(), error.getDefaultMessage())
            );
            String errorMsg= "";

            for (String key: errors.keySet()) {
                errorMsg += "Lỗi tại property : " + key + " - Lý do xảy ra : " + errors.get(key) + "\n";
            }
            throw new InputNotAccept(errorMsg) ;
        }
        CDiceServices getUser = CUserDiceServices.findUser(userInfo.getUsername());

        if (getUser != null) {
            ConsumptionProbe probe = getUser.getLimitService().getBucket().tryConsumeAndReturnRemaining(1);
            if (probe.isConsumed()) {
                getUser.newRoll();
                return getUser.getResultObj();
            }else {
                throw new TooManyRequest("Chơi nhiều quá ! Thử lại sau " + String.valueOf(probe.getNanosToWaitForRefill()/1000000000) + " giây nữa nhé");
            }
        } else {
            CUserDiceServices.addUser(userInfo.getUsername(),userInfo.getFirstname(),userInfo.getLastname());
            getUser = CUserDiceServices.findUser(userInfo.getUsername());
            getUser.newRoll();
            return getUser.getResultObj();
        }
    }

    @GetMapping("/all-history")
    public List <CDiceResult> getAllResult (@RequestParam (required = true) String username) throws Exception {
        CDiceServices getUser = CUserDiceServices.findUser(username);
        if (username.equals("")) {
            throw new InputNotAccept("Username không được bỏ trống");
        }
        if (getUser != null) {
            List <CDiceResult> allResult = getUser.getResultList();
            if (allResult.size() == 0) {
                throw new ResourceNotFound("User có trong hệ thống nhưng không có data tồn tại");
            } else {return allResult;}
        } else {
            throw new ResourceNotFound("User không tồn tại trong hệ thống");
        }
    }

    @GetMapping("/dice-history")
    public Map <String,Object> getAllDice (@RequestParam (required = true) String username) throws Exception {
        Map <String,Object> allDiceResult = new HashMap<>();
        CDiceServices getUser = CUserDiceServices.findUser(username);
        if (username.equals("")) {
            throw new InputNotAccept("Username không được bỏ trống");
        }
        if (getUser != null) {
            List <Integer> allDiceObj = getUser.getAllDice();
            if (allDiceObj.size() == 0) {
                throw new ResourceNotFound("User có trong hệ thống nhưng không có data tồn tại");
            } else {
                allDiceResult.put("dices", allDiceObj);
                return allDiceResult;
            }
        } else {
            throw new ResourceNotFound("User không tồn tại trong hệ thống");
        }
    }

    @GetMapping("/prize-history")
    public Map <String,Object> getAllPrize (@RequestParam (required = true) String username) throws Exception {
        Map <String,Object> allPrizeResult = new HashMap<>();
        CDiceServices getUser = CUserDiceServices.findUser(username);
        if (username.equals("")) {
            throw new InputNotAccept("Username không được bỏ trống");
        }
        if (getUser != null) {
            List <String> allPrizeObj = getUser.getAllPrize();
            if (allPrizeObj.size() == 0) {
                throw new ResourceNotFound("User có trong hệ thống nhưng không có data tồn tại");
            } else {
                allPrizeResult.put("prizes", allPrizeObj);
                return allPrizeResult;
            }
        } else {
            throw new ResourceNotFound("User không tồn tại trong hệ thống");
        }
    }

    @GetMapping("/voucher-history")
    public Map <String,Object> getAllVoucher (@RequestParam (required = true) String username) throws Exception {
        Map <String,Object> allVoucherResult = new HashMap<>();
        CDiceServices getUser = CUserDiceServices.findUser(username);
        if (username.equals("")) {
            throw new InputNotAccept("Username không được bỏ trống");
        }
        if (getUser != null) {
            List <CVoucher> allVoucherObj = getUser.getAllVoucher();
            if (allVoucherObj.size() == 0) {
                throw new ResourceNotFound("User có trong hệ thống nhưng không có data tồn tại");
            } else {
                allVoucherResult.put("vouchers", allVoucherObj);
                return allVoucherResult;
            }
        } else {
            throw new ResourceNotFound("User không tồn tại trong hệ thống");
        }
    }

    
}
