package com.devcamp.apilist.allapilist.controller;

import java.util.List;

import com.devcamp.apilist.allapilist.model.CDrink;
import com.devcamp.apilist.allapilist.services.CDrinkServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin (value = "*", maxAge = -1)
@RequestMapping("/drink-api")
@RestController
public class CDrinkController {
    @Autowired
    static CDrinkServices drinks;

    @GetMapping ("/all")
    public List <CDrink> getallDrinks () throws Exception {
        List <CDrink> allDrinks = CDrinkServices.getDrinkList();
        return allDrinks;
    }
}
