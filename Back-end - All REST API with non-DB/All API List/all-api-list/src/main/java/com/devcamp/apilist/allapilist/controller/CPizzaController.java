package com.devcamp.apilist.allapilist.controller;

import java.time.Duration;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.devcamp.apilist.allapilist.enumlist.EPizzaStatus;
import com.devcamp.apilist.allapilist.interfacelist.CThrowErrorInterface;
import com.devcamp.apilist.allapilist.model.CPizzaInputOrder;
import com.devcamp.apilist.allapilist.model.CPizzaOrder;
import com.devcamp.apilist.allapilist.model.CPizzaType;
import com.devcamp.apilist.allapilist.model.CUpdateOrder;
import com.devcamp.apilist.allapilist.services.CPizza365Services;
import com.devcamp.apilist.allapilist.services.CPizzaTypeService;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.github.bucket4j.Bandwidth;
import io.github.bucket4j.Bucket;
import io.github.bucket4j.Bucket4j;
import io.github.bucket4j.ConsumptionProbe;
import io.github.bucket4j.Refill;

@RestController
@CrossOrigin (value = "*",maxAge = -1)
@RequestMapping("/pizza-365-api")
public class CPizzaController implements CThrowErrorInterface{

    @GetMapping("/order")
    public List <CPizzaOrder> getAllOrder (@RequestParam (required = false , defaultValue = "", name = "orderId") String orderId ) throws Exception{
        if (orderId == null || orderId.trim().equals("")) {
            return CPizza365Services.getPizzaList();
        } else {
            List <CPizzaOrder> listPizzaFound = new ArrayList<CPizzaOrder>();
            CPizzaOrder pizzaOrder = CPizza365Services.findOrderByOrderId(orderId);
            if (pizzaOrder != null) {
                listPizzaFound.add(CPizza365Services.findOrderByOrderId(orderId));
                return listPizzaFound;
            } else {
                throw new ResourceNotFound("Không tìm thấy đơn hàng mang Order Id yêu cầu");
            }
        }
    }

    Instant firstRefillTime = ZonedDateTime.now()
        .truncatedTo(ChronoUnit.DAYS)
        .plus(0, ChronoUnit.DAYS)
        .toInstant();
    Bandwidth limit = Bandwidth.classic(100, Refill.intervallyAligned(100, Duration.ofDays(1),firstRefillTime,true));
    private final Bucket bucket = Bucket4j.builder().addLimit(limit).build() ;

    @PostMapping(value = "/order") 
    public CPizzaOrder createNewOrder (@Valid @RequestBody (required = true) CPizzaInputOrder orderDetail , BindingResult allError) throws Exception {
        if (allError.hasErrors()) {
            Map<String, String> errors = new HashMap <> ();
            allError.getFieldErrors().forEach(
                    error -> errors.put(error.getField(), error.getDefaultMessage())
            );
            String errorMsg= "";

            for (String key: errors.keySet()) {
                errorMsg += "Lỗi tại property : " + key + " - Lý do xảy ra : " + errors.get(key) + "\n";
            }
            throw new InputNotAccept(errorMsg) ;
        }
        ConsumptionProbe probe = bucket.tryConsumeAndReturnRemaining(1);
        if (probe.isConsumed()) {
            CPizzaOrder newOrder = new CPizzaOrder(orderDetail);
            CPizza365Services.getPizzaList().add(newOrder);
            return newOrder;
        } else {
            throw new TooManyRequest("Đặt nhiều đơn quá ! Đặt lại sau " + String.valueOf(probe.getNanosToWaitForRefill()/1000000000/3600) + " tiếng nữa nhé");
        }

    }

    @PutMapping (value = "/order")
    public CPizzaOrder updateOrder (@Valid @RequestBody (required = true) CUpdateOrder status,BindingResult allError,@RequestParam (required = true, name = "id") Integer id) throws Exception {
        if (allError.hasErrors()) {
            Map<String, String> errors = new HashMap <> ();
            allError.getFieldErrors().forEach(
                    error -> errors.put(error.getField(), error.getDefaultMessage())
            );
            String errorMsg= "";

            for (String key: errors.keySet()) {
                errorMsg += "Lỗi tại property : " + key + " - Lý do xảy ra : " + errors.get(key) + "\n";
            }
            throw new InputNotAccept(errorMsg) ;
        }

        if (CPizza365Services.findOrderById(id) == null) {
            throw new ResourceNotFound("Không tìm thấy đơn hàng theo id yêu cầu");
        } else {
            CPizzaOrder thisOrder = CPizza365Services.findOrderById(id);
            if (status.getTrangThai() == thisOrder.getTrangThai()) {
                throw new ConflictData("Trạng thái cần sửa trùng với trạng thái ban đầu");
            }
            String newStatus = status.getTrangThai().toString();
            thisOrder.setTrangThai(EPizzaStatus.valueOf(newStatus));
            thisOrder.setNgayCapNhat(new Date().getTime());
            return thisOrder;
        }
    }

    @DeleteMapping (value = "/order")
    public Map <String,String> deleteOrder (@RequestParam (required = true, name = "id") Integer id) throws Exception {
        Map <String,String> newResponse = new HashMap<>();
        if (CPizza365Services.findOrderById(id) == null) {
            throw new ResourceNotFound("Không tìm thấy đơn hàng theo id yêu cầu");
        } else {
            CPizzaOrder thisOrder = CPizza365Services.findOrderById(id);
            List <CPizzaOrder> allList = CPizza365Services.getPizzaList();
            allList.remove(thisOrder);
            newResponse.put ("status","Thành công");
            return newResponse;
        }
    }

    @GetMapping (value = "/pizza-type")
    public List <CPizzaType> getPizzaTypeList () {
        return CPizzaTypeService.getTypeList();
    }

    @GetMapping (value = "/special-today-type")
    public List <CPizzaType> getTodayPizzaTypeList () {
        return CPizzaTypeService.getRandomType();
    }
}
