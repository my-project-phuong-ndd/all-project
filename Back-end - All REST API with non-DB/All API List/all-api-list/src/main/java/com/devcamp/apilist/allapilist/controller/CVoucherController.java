package com.devcamp.apilist.allapilist.controller;

import java.util.List;

import com.devcamp.apilist.allapilist.interfacelist.CThrowErrorInterface;
import com.devcamp.apilist.allapilist.model.CVoucher;
import com.devcamp.apilist.allapilist.services.CVoucherServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin (value = "*",maxAge = -1)
@RestController
@RequestMapping("/voucher-api")
public class CVoucherController implements CThrowErrorInterface{
    @Autowired
    CVoucherServices allVoucher;


    @RequestMapping(value = "/voucher-all",method = RequestMethod.GET)
    public List <CVoucher> getAllVoucher () {
        List <CVoucher> allVoucher = CVoucherServices.getVoucherList();
        return allVoucher;
    }

    @GetMapping("/voucher-detail/{voucherId}")
    public CVoucher detailVoucher (@PathVariable (required = true) Integer voucherId) throws Exception {
        CVoucher getVoucher = CVoucherServices.findVoucherById(voucherId);
        if (getVoucher != null) {
            return getVoucher;
        } else {
            throw new ResourceNotFound("Không tìm thấy Voucher theo yêu cầu");
        }
    }
}
