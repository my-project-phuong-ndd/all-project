package com.devcamp.apilist.allapilist.enumlist;

import java.util.ArrayList;

public enum EMenuPizza {
    S,M,L;

    public static  boolean contains(String test) {

        for (EMenuPizza c : EMenuPizza.values()) {
            if (c.name().equals(test)) {
                return true;
            }
        }
        return false;
    }

    public static final String createRexExp () {
        EMenuPizza[] enumValues = EMenuPizza.values();
        ArrayList <String> enumList = new ArrayList<>();
        for (EMenuPizza each : enumValues) {
            enumList.add(String.format("^%s$", each.name()));
        }
        String result = String.join("|", enumList);
        return result.toString();
    }

}
