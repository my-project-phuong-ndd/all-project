package com.devcamp.apilist.allapilist.enumlist;

public enum EPizzaStatus {
    confirmed,cancel,open;

    public static boolean contains(String test) {

        for (EPizzaStatus c : EPizzaStatus.values()) {
            if (c.name().equals(test)) {
                return true;
            }
        }
        return false;
    }
}
