package com.devcamp.apilist.allapilist.enumlist;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public enum EPrizeDice {
    Oto("Ô tô"),XeMay("Xe máy"),Mu("Mũ"),Ao("Áo");


    private static final List <EPrizeDice> VALUES = Arrays.asList(values());
    private static final int SIZE = VALUES.size();
    private static final Random RANDOM = new Random();

    public static EPrizeDice randomPrize() {
        return VALUES.get(RANDOM.nextInt(SIZE));
    }

    public final String label;

    EPrizeDice(String label) {
        this.label = label;
    }

}
