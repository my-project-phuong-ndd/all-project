package com.devcamp.apilist.allapilist.enumlist;

public enum ETypePizza {
    bacon,seafood,hawaii;
    public static boolean contains(String test) {

        for (ETypePizza c : ETypePizza.values()) {
            if (c.name().equals(test)) {
                return true;
            }
        }
        return false;
    }
}
