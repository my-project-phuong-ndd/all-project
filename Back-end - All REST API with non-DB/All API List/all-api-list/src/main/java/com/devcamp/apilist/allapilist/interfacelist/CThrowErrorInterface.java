package com.devcamp.apilist.allapilist.interfacelist;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public interface CThrowErrorInterface {
    @ResponseStatus (value = HttpStatus.NOT_FOUND)
    public class ResourceNotFound extends Exception {
        public ResourceNotFound(String errorMessage) {
            super(errorMessage);
        }
    }


    @ResponseStatus (value = HttpStatus.NOT_ACCEPTABLE)
    public class InputNotAccept extends Exception {
        public InputNotAccept(String errorMessage) {
            super(errorMessage);
        }
    }

    @ResponseStatus (value = HttpStatus.TOO_MANY_REQUESTS)
    public class TooManyRequest extends Exception {
        public TooManyRequest(String errorMessage) {
            super(errorMessage);
        }
    }

    @ResponseStatus (value = HttpStatus.CONFLICT)
    public class ConflictData extends Exception {
        public ConflictData(String errorMessage) {
            super(errorMessage);
        }
    }
}
