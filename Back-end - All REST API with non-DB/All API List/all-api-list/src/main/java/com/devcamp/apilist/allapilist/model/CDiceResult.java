package com.devcamp.apilist.allapilist.model;

import java.util.List;

import com.devcamp.apilist.allapilist.enumlist.EPrizeDice;
import com.devcamp.apilist.allapilist.services.CVoucherServices;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class CDiceResult implements Cloneable{
    @JsonIgnore
    private Integer bonusPrize = 0;
    private CVoucher voucher;
    private Integer dice;
    private String prize;
    
    // Dùng để ẩn property bonusPrize khi xuất ra ngoài API (Để protected hoặc private), để public nếu muốn public ra
    public Integer getBonusPrize () {
        return this.bonusPrize;
    }

    public CDiceResult (CDiceResult that) {
        this(that.getBonusPrize(),that.getVoucher(), that.getDice(), that.getPrize());
    }

    
    public void newResult() {
        this.dice = new CRandomId().randomNumberic(1, 6);
        if (this.dice > 3) {
            ++ this.bonusPrize;
            List <CVoucher> allVoucher = CVoucherServices.getVoucherList();
            Integer voucherSize = allVoucher.size();
            this.voucher = allVoucher.get(new CRandomId().randomNumberic(0,voucherSize - 1));
        } else {
            this.voucher = null;
            this.bonusPrize = 0;
        } 

        if (this.bonusPrize >= 3) {
            this.prize = EPrizeDice.randomPrize().label;
        } else {
            this.prize = null;
        }
    }

}
