package com.devcamp.apilist.allapilist.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor @RequiredArgsConstructor
public class CDrink {
    @NonNull private String maNuocUong;
    @NonNull private String tenNuocUong;
    @NonNull private Integer donGia;
    private final Long ngayTao = new Date().getTime();
    private Long ngayCapNhat = new Date().getTime();
}
