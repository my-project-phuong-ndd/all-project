package com.devcamp.apilist.allapilist.model;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.devcamp.apilist.allapilist.annotation.ValueOfArray;
import com.devcamp.apilist.allapilist.annotation.ValueOfEnum;
import com.devcamp.apilist.allapilist.enumlist.EMenuPizza;
import com.devcamp.apilist.allapilist.enumlist.ETypePizza;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor
public class CPizzaInputOrder {

    @NotBlank (message = "Pizza Size không được bỏ trống")
    @ValueOfEnum (enumClass = EMenuPizza.class, message = "Pizza Size bắt buộc phải là S,M,L")
    private final String kichCo;

    @NotNull (message = "Đường kính không được bỏ trống")
    @DecimalMin(value = "0.0", message = "Đường kính phải là số nguyên lớn hơn {value}") 
    private final Integer duongKinh;

    @NotNull (message = "Sườn không được bỏ trống")
    @DecimalMin(value = "0.0", message = "Sườn phải là số nguyên lớn hơn {value}") 
    private final Integer suon;

    @NotNull (message = "Salad không được bỏ trống")
    @DecimalMin(value = "0.0", message = "Salad phải là số nguyên lớn hơn {value}") 
    private final Integer salad;

    @DecimalMin(value = "0.0", message = "ID Voucher phải là số nguyên lớn hơn {value}") 
    private final Integer idVourcher;

    @NotNull (message = "Thành tiền không được bỏ trống")
    @DecimalMin(value = "0.0", message = "Thành tiền phải là số nguyên lớn hơn {value}") 
    private final Integer thanhTien;

    @NotBlank (message = "Id nước uống không được bỏ trống")
    @ValueOfArray (arrayClass = {"COCA","PEPSI","FANTA","TRATAC","LAVIE","TRASUA"} , message = "ID nước uống phải là một trong các giá trị sau {arrayClass}")
    private final String idLoaiNuocUong;

    @NotNull (message = "Số lượng nước không được bỏ trống")
    @DecimalMin(value = "0.0", message = "Số lượng nước phải là số nguyên lớn hơn {value}") 
    private final Integer soLuongNuoc;

    @NotBlank (message = "Kích cỡ không được bỏ trống")
    @ValueOfEnum (enumClass = ETypePizza.class, message = "Pizza Size phải là một trong các loại : bacon, seafood, hawaii")
    private final String loaiPizza;

    @NotBlank (message = "Họ tên không được bỏ trống")
    @Pattern (regexp = "^([\\p{L}\\s]+[\\p{L}]\\s?)*$",message = "Họ và tên chỉ được gồm chữ Latin (Chấp nhận có dấu tên VN), khoảng trắng, ngoài ra không có số và các kí tự đặc biệt")
    private final String hoTen;

    @Email (message = "Email không hợp lệ")
    private String email = "";

    @NotBlank (message = "Số điện thoại không được bỏ trống")
    @Length (min = 10, max=12 , message = "Số điện thoại chỉ có đúng 10 - 12 ký tự")
    @Pattern (regexp = "^((\\+*84|[0]{1})+[35789]{1}[0-9]{8})*$",message = "Số điện thoại là 10 số (Nếu bắt đầu bằng 0) hoặc 11 số (bắt đầu bằng 84 hoặc +84) , thuộc nhà mạng VN (03,05,07,08,09)")
    private final String soDienThoai;

    @NotBlank (message = "Địa chỉ không được bỏ trống")
    private final String diaChi;

    private String loiNhan = "";

    public CPizzaInputOrder(CPizzaInputOrder newOrder) {
        this.kichCo = newOrder.getKichCo();
        this.duongKinh = newOrder.getDuongKinh();
        this.suon = newOrder.getSuon();
        this.salad = newOrder.getSalad();
        this.idVourcher = newOrder.getIdVourcher();
        this.thanhTien = newOrder.getThanhTien();
        this.idLoaiNuocUong = newOrder.getIdLoaiNuocUong();
        this.soLuongNuoc = newOrder.getSoLuongNuoc();
        this.loaiPizza = newOrder.getLoaiPizza();
        this.hoTen = newOrder.getHoTen();
        this.email = newOrder.getEmail();
        this.soDienThoai = newOrder.getSoDienThoai();
        this.diaChi = newOrder.getDiaChi();
        this.loiNhan = newOrder.getLoiNhan();
    }
    
    public static CPizzaInputOrder newOrder1 = new CPizzaInputOrder("S", 20, 2, 300, 12354, 200000, "PEPSI", 2, "seafood", "Thành", "thanh@gmail.com", "0909090909", "Nhà", "test"); 
}
