package com.devcamp.apilist.allapilist.model;

import java.util.Date;

import com.devcamp.apilist.allapilist.enumlist.EPizzaStatus;
import com.devcamp.apilist.allapilist.services.CPizza365Services;
import com.devcamp.apilist.allapilist.services.CVoucherServices;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CPizzaOrder extends CPizzaInputOrder {
    
    private static Integer newId = 0;
    private final Integer id = ++ newId;
    private final String orderId = CPizzaOrder.getUniqueOrderId();
    private Integer giamGia = CVoucherServices.findVoucherById(this.getIdVourcher()) != null ? CVoucherServices.findVoucherById(this.getIdVourcher()).getPhanTramGiamGia() * this.getThanhTien()/100 : 0 ;
    private final Long ngayTao = new Date().getTime();
    private Long ngayCapNhat = new Date().getTime();
    private EPizzaStatus trangThai = EPizzaStatus.open;

    public CPizzaOrder (CPizzaInputOrder newOrder) {
        super (newOrder);
    }

    private static String getUniqueOrderId () {
        String newOrderId = new CRandomId().randomAlphaNumeric(10);
        CPizzaOrder isFound = CPizza365Services.findOrderByOrderId(newOrderId);
        if (isFound != null) {
            return getUniqueOrderId();
        } else {
            return newOrderId;
        }
    }
}
