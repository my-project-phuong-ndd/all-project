package com.devcamp.apilist.allapilist.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor
public class CPizzaType {
    private String id;
    private String title;
    private String titleDescrition;
    private String description;
}
