package com.devcamp.apilist.allapilist.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;

// Phải import thêm dependency trong pom.xml
import org.apache.commons.lang3.RandomStringUtils;
 
public class CRandomId {
 
    private static final String alpha = "abcdefghijklmnopqrstuvwxyz"; // a-z
    private static final String alphaUpperCase = alpha.toUpperCase(); // A-Z
    private static final String digits = "0123456789"; // 0-9
    private static final String specials = "~=+%^*/()[]{}/!@#$?|";
    private static final String ALPHA_NUMERIC = alpha + alphaUpperCase + digits;
    private static final String ALL = alpha + alphaUpperCase + digits + specials;
 
    private static Random generator = new Random();
 

    public int randomNumberic (int min, int max) {
        return generator.nextInt((max - min) + 1) + min;
    }
    /**
     * Random string with a-zA-Z0-9, not included special characters
     */
    public String randomAlphaNumeric(int numberOfCharactor) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < numberOfCharactor; i++) {
            int number = randomNumber(0, ALPHA_NUMERIC.length() - 1);
            char ch = ALPHA_NUMERIC.charAt(number);
            sb.append(ch);
        }
        return sb.toString();
    }
 
    /**
     * Random string password with at least 1 digit and 1 special character
     */
    public String randomPassword(int numberOfCharactor) {
        List<String> result = new ArrayList<>();
        Consumer<String> appendChar = s -> {
            int number = randomNumber(0, s.length() - 1);
            result.add("" + s.charAt(number));
        };
        appendChar.accept(digits);
        appendChar.accept(specials);
        while (result.size() < numberOfCharactor) {
            appendChar.accept(ALL);
        }
        Collections.shuffle(result, generator);
        return String.join("", result);
    }
 
    public static int randomNumber(int min, int max) {
        return generator.nextInt((max - min) + 1) + min;
    }

    public static void testRandom () {
        System.out.println(RandomStringUtils.random(4)); // 
        System.out.println(RandomStringUtils.random(6)); // 㚔쬩́㽩
  
        System.out.println(RandomStringUtils.randomAscii(4)); // qe51
        System.out.println(RandomStringUtils.randomAscii(6)); // MqQ^X\
  
        System.out.println(RandomStringUtils.randomNumeric(4)); // 9808
        System.out.println(RandomStringUtils.randomNumeric(6)); // 338756
  
        System.out.println(RandomStringUtils.randomAlphabetic(4)); // kvMu
        System.out.println(RandomStringUtils.randomAlphabetic(6)); // PeykyQ
  
        System.out.println(RandomStringUtils.randomAlphanumeric(4)); // MavC
        System.out.println(RandomStringUtils.randomAlphanumeric(6)); // fR2BEf
  
        String input = "abcd1234!@#$%^&*()-=_+;:<>,.?/";
        System.out.println(RandomStringUtils.random(4, input)); // 1)+(
        System.out.println(RandomStringUtils.random(6, input)); // -c=,a,
    }
}