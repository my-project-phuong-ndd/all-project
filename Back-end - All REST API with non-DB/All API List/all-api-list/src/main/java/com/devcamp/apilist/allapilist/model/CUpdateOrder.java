package com.devcamp.apilist.allapilist.model;

import javax.validation.constraints.NotNull;

import com.devcamp.apilist.allapilist.annotation.EnumNamePattern;
import com.devcamp.apilist.allapilist.enumlist.EPizzaStatus;

import lombok.Data;

@Data
public class CUpdateOrder {
    @NotNull
    @EnumNamePattern (regexp = "confirmed|cancel",message = "Trang thái phải là 1 trong 2 giá trị confirmed,cancel, không có trạng thái open")
    private EPizzaStatus trangThai = EPizzaStatus.valueOf("open");
}
