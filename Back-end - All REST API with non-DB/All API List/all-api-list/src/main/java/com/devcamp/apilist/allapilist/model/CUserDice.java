package com.devcamp.apilist.allapilist.model;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor @Getter @Setter
public class CUserDice {
    @NotBlank (message = "Username không được bỏ trống")
    private String username;
    @NotBlank (message = "Firstname không được bỏ trống")
    private String firstname;
    @NotBlank (message = "Lastname không được bỏ trống")
    private String lastname;

    public CUserDice (String username) {
        this.username = username;
        this.firstname = "checked1";
        this.lastname = "checked2";
    }
}
