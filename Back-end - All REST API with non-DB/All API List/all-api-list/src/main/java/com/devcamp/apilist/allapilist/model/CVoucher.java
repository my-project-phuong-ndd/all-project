package com.devcamp.apilist.allapilist.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class CVoucher {
    private static Integer newId = 0;
    private final Integer id = ++ newId;
    private Integer maVoucher;
    private Integer phanTramGiamGia;
    private final Long ngayTao = new Date().getTime();
    private final Long ngayCapNhat = new Date().getTime();
}
