package com.devcamp.apilist.allapilist.services;

import java.util.ArrayList;
import java.util.List;

import com.devcamp.apilist.allapilist.model.CDiceResult;
import com.devcamp.apilist.allapilist.model.CUserDice;
import com.devcamp.apilist.allapilist.model.CVoucher;

import org.springframework.beans.factory.annotation.Autowired;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter 
public class CDiceServices extends CUserDice   {
    private final CDiceResult resultObj = new CDiceResult();
    private final List <CDiceResult> resultList = new ArrayList<>();
    private final CLimitServices limitService = new CLimitServices();

    public CDiceServices(String username) {
        super(username);
    }

    @Autowired
    public CDiceServices(String username, String firstname, String lastname) {
        super(username, firstname, lastname);
    }

    public void newRoll () {
        resultObj.newResult();
        resultList.add(new CDiceResult(resultObj));
    }

    public CLimitServices getLimitService () {
        return limitService;
    }

    public CDiceResult getResultObj() {
        return resultObj;
    }

    public List <CDiceResult> getResultList() {
        return resultList;
    }

    public List <Integer> getAllDice() {
        List <Integer> newList = new ArrayList<Integer>();
        List <CDiceResult> allResult = this.getResultList();
        for (int i = 0 ; i < allResult.size(); ++i) {
            newList.add(allResult.get(i).getDice());
        }
        return newList;
    }

    public List <String> getAllPrize() {
        List <String> newList = new ArrayList<String>();
        List <CDiceResult> allResult = this.getResultList();
        for (int i = 0 ; i < allResult.size(); ++i) {
            if (allResult.get(i).getPrize() != null) {
                newList.add(allResult.get(i).getPrize());
            }
        }
        return newList;
    }

    public List <CVoucher> getAllVoucher() {
        List <CVoucher> newVoucherList = new ArrayList<CVoucher>();
        List <CDiceResult> allResult = this.getResultList();
        for (int i = 0 ; i < allResult.size(); ++i) {
            if (allResult.get(i).getVoucher() != null) {
                newVoucherList.add(allResult.get(i).getVoucher());
            }
        }
        return newVoucherList;
    }
}
