package com.devcamp.apilist.allapilist.services;

import java.util.ArrayList;
import java.util.List;

import com.devcamp.apilist.allapilist.model.CDrink;

import org.springframework.stereotype.Service;

@Service 
public class CDrinkServices {
    private static List <CDrink> drinkList = new ArrayList<CDrink>();

    static {
        CDrink coca = new CDrink("COCA", "Cocacola", 15000);
        CDrink pepsi = new CDrink("PEPSI", "Pepsi", 15000);
        CDrink tratac = new CDrink("TRATAC", "Trà tắc", 10000);
        CDrink fanta = new CDrink("FANTA", "Fanta", 15000);
        CDrink trasua = new CDrink("TRASUA", "Trà sữa trân châu", 40000);
        CDrink lavie = new CDrink("LAVIE", "Lavie ", 5000);

        getDrinkList().add(coca);
        getDrinkList().add(pepsi);
        getDrinkList().add(tratac);
        getDrinkList().add(fanta);
        getDrinkList().add(trasua);
        getDrinkList().add(lavie);
    }

    public static List <CDrink> getDrinkList() {
        return drinkList;
    }

    public static void setDrinkList(List <CDrink> drinkList) {
        CDrinkServices.drinkList = drinkList;
    }

    public static final String [] getDrinkArray () {
        String [] newResult = new String [drinkList.size()];
        for (int i =0; i < drinkList.size(); ++i) {
            newResult [i] = drinkList.get(i).getMaNuocUong();
        }
        return newResult;
    }
}
