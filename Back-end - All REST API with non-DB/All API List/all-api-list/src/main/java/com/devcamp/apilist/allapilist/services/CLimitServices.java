package com.devcamp.apilist.allapilist.services;

import java.time.Duration;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

import io.github.bucket4j.Bandwidth;
import io.github.bucket4j.Bucket;
import io.github.bucket4j.Bucket4j;
import io.github.bucket4j.Refill;
import lombok.Getter;

@Getter
public final class CLimitServices {
    Instant firstRefillTime = ZonedDateTime.now()
        .truncatedTo(ChronoUnit.HOURS)
        .plus(0, ChronoUnit.HOURS)
        .toInstant();
    Bandwidth limiMinute = Bandwidth.simple(10, Duration.ofMinutes(1));
    Bandwidth limitHours = Bandwidth.classic(12, Refill.greedy(50, Duration.ofHours(1)));
    Bandwidth limit3Hour = Bandwidth.classic(13, Refill.intervallyAligned(70, Duration.ofHours(3), firstRefillTime, true));
    Bandwidth limitDays = Bandwidth.classic(14, Refill.intervally(150, Duration.ofDays(1)));
    private final Bucket bucket = Bucket4j.builder().addLimit(limiMinute).addLimit(limitHours).addLimit(limit3Hour).addLimit(limitDays).build();

    public CLimitServices () {
    }
}
