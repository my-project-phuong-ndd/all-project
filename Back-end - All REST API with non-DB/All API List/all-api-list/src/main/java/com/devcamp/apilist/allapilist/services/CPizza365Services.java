package com.devcamp.apilist.allapilist.services;

import java.util.ArrayList;
import java.util.List;

import com.devcamp.apilist.allapilist.model.CPizzaInputOrder;
import com.devcamp.apilist.allapilist.model.CPizzaOrder;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter 
public class CPizza365Services {
    private static List <CPizzaOrder> pizzaList = new ArrayList<CPizzaOrder>();

    static {
        pizzaList.add(new CPizzaOrder(CPizzaInputOrder.newOrder1));
        pizzaList.add(new CPizzaOrder(CPizzaInputOrder.newOrder1));
        pizzaList.add(new CPizzaOrder(CPizzaInputOrder.newOrder1));
    }

    public static CPizzaOrder findOrderByOrderId (String orderId) {
        for (CPizzaOrder list : pizzaList) {
            if (list.getOrderId().equals(orderId)){
                return list;
            }
        }
        return null;
    }

    public static CPizzaOrder findOrderById (Integer id) {
        for (CPizzaOrder list : pizzaList) {
            if (list.getId().equals(id)){
                return list;
            }
        }
        return null;
    }

    public static Integer findIndexById (Integer id) {
        for (CPizzaOrder list : pizzaList) {
            if (list.getId().equals(id)){
                return pizzaList.indexOf(list);
            }
        }
        return null;
    }

    public static List <CPizzaOrder> getPizzaList () {
        return pizzaList;
    }

}
