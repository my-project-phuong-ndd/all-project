package com.devcamp.apilist.allapilist.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.devcamp.apilist.allapilist.model.CPizzaType;

public class CPizzaTypeService {
    private static List<CPizzaType> typeList = new ArrayList<CPizzaType>();

    static {
        typeList.add(new CPizzaType("seafood", "OCEAN MANIA", "Pizza hải sản xốt mayonnaise", "Xốt cà chua, phô mai mozzarella, tôm, mực, thanh cua, hành tây"));
        typeList.add(new CPizzaType("bacon", "CHEESY CHICKEN BACON", "Pizza gà phô mai thịt heo xông khói", "Xốt phô mai, thịt gà, thịt heo muối, phô mai mozzarella, cà chua."));
        typeList.add(new CPizzaType("hawaii", "HAWAIIAN", "Pizza dăm bông dừa kiểu hawaii", "Xốt cà chua, phô mai mozzarella, thịt dăm bông, thơm."));
        typeList.add(new CPizzaType("meat", "MEAT AND SAUSAGE", "Pizza thịt và xúc xích", "Xốt cà chua, phô mai mozzarella, thịt xông khói, xúc xích, thịt bò, giăm bông và peperoni"));
        typeList.add(new CPizzaType("pepperoni", "PREMIUM PEPPERONI", "Pizza pepperoni thương hạng", "Xốt cà chua, phô mai mozzarella, pepperoni chất lượng cao"));
        typeList.add(new CPizzaType("mixed", "MIXED PIZZA", "Pizza thập cẩm", "Xốt cà chua, phô mai mozzarella,thịt bò, thịt xông khói, pepperoni, ớt chuông, nấm và hành tây,"));
        typeList.add(new CPizzaType("chicken", "CHICKEN AND MUSHROOM", "Pizza gà nướng nấm", "Xốt tiêu đen, phô mai mozzarella, thịt gà, nấm, thơm, cà rốt và rau mầm"));
        typeList.add(new CPizzaType("tuna", "TUNA FISH", "Pizza cá ngừ", "Xốt pesto, phô mai mozzarella, cá ngừ, thanh cua, hành tây, thơm"));
        typeList.add(new CPizzaType("cheese", "PREMIUM CHEESE", "Pizza phô mai cao cấp", "Xốt cà chua, phô mai mozzarella 3 lớp"));
        typeList.add(new CPizzaType("shrimp", "SHRIMP BUTTER", "Pizza tôm xốt bơ tỏi", "Xốt bơ tỏi, phô mai mozzarella, tôm, hành tây"));
        typeList.add(new CPizzaType("vegetable", "VEGETABLE PIZZA", "Pizza rau củ xốt bơ tỏi", "Xốt cà chua, phô mai mozzarella,ô liu đen, cà chua bi tươi ngon, nấm, thơm, bắp, hành tây"));
        typeList.add(new CPizzaType("bbq", "OCEAN MANIA", "Pizza hải sản xốt mayonnaise", "Xốt hàn quốc, phô mai mozzarella,thịt bò úc, thơm, phủ rau mầm và mè rang"));
    }

    private static List <CPizzaType> shuffleList = new ArrayList<CPizzaType>(typeList);

    public static List<CPizzaType> getTypeList () {
        return typeList;
    }

    public static List <CPizzaType> getRandomType () {
        Collections.shuffle(shuffleList);
        return new ArrayList<>(shuffleList.subList(0, 3));
    }
}
