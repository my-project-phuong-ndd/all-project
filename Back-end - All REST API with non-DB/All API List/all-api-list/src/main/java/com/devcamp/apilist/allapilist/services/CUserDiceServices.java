package com.devcamp.apilist.allapilist.services;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

@Getter
public class CUserDiceServices {
    private static List <CDiceServices> userDiceList = new ArrayList<CDiceServices>();
    
    public static List <CDiceServices> getUserDiceList () {
        return CUserDiceServices.userDiceList;
    }

    // For testing code only
    static {
        userDiceList.add(new CDiceServices("test1","test","test"));
        userDiceList.add(new CDiceServices("test2","test","test"));
        for (int i = 0 ; i < 150 ; ++i) {
            userDiceList.get(0).newRoll();
            userDiceList.get(1).newRoll();
        }
        userDiceList.add(new CDiceServices("test3","test","test"));
    }

    public static CDiceServices findUser (String username) {
        for (int i = 0 ; i < CUserDiceServices.getUserDiceList().size();++i) {
            if (CUserDiceServices.getUserDiceList().get(i).getUsername().equals(username)) {
                return CUserDiceServices.getUserDiceList().get(i);
            }
        }
        return null;
    }

    public static void addUser (String username,String firstname,String lastname) {
        if (CUserDiceServices.findUser(username) == null) {
            CUserDiceServices.getUserDiceList().add(new CDiceServices(username,firstname,lastname));
        }
    }

}
