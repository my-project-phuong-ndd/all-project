package com.devcamp.apilist.allapilist.services;

import java.util.ArrayList;
import java.util.List;

import com.devcamp.apilist.allapilist.model.CVoucher;

import org.springframework.stereotype.Service;

@Service
public class CVoucherServices {
    private static List <CVoucher> voucherList = new ArrayList<CVoucher>();

    static {
        getVoucherList().add(new CVoucher(16512, 10));
        getVoucherList().add(new CVoucher(12354, 10));
        getVoucherList().add(new CVoucher(12332, 10));
        getVoucherList().add(new CVoucher(95531, 10));
        getVoucherList().add(new CVoucher(81432, 10));
        getVoucherList().add(new CVoucher(15746, 10));
        getVoucherList().add(new CVoucher(76241, 10));
        getVoucherList().add(new CVoucher(64562, 10));
        getVoucherList().add(new CVoucher(25896, 10));
        getVoucherList().add(new CVoucher(85423, 10));
        getVoucherList().add(new CVoucher(87654, 20));
        getVoucherList().add(new CVoucher(86423, 20));
        getVoucherList().add(new CVoucher(46253, 20));
        getVoucherList().add(new CVoucher(46211, 20));
        getVoucherList().add(new CVoucher(36594, 20));
        getVoucherList().add(new CVoucher(24864, 20));
        getVoucherList().add(new CVoucher(23156, 20));
        getVoucherList().add(new CVoucher(13946, 20));
        getVoucherList().add(new CVoucher(34962, 20));
        getVoucherList().add(new CVoucher(26491, 20));
        getVoucherList().add(new CVoucher(94634, 30));
        getVoucherList().add(new CVoucher(87643, 30));
        getVoucherList().add(new CVoucher(61353, 30));
        getVoucherList().add(new CVoucher(64532, 30));
        getVoucherList().add(new CVoucher(89436, 30));
        getVoucherList().add(new CVoucher(73256, 30));
        getVoucherList().add(new CVoucher(21561, 30));
        getVoucherList().add(new CVoucher(35468, 30));
        getVoucherList().add(new CVoucher(32486, 30));
        getVoucherList().add(new CVoucher(96462, 30));
        getVoucherList().add(new CVoucher(41356, 40));
        getVoucherList().add(new CVoucher(76164, 40));
        getVoucherList().add(new CVoucher(72156, 40));
        getVoucherList().add(new CVoucher(46594, 40));
        getVoucherList().add(new CVoucher(35469, 40));
        getVoucherList().add(new CVoucher(34546, 40));
        getVoucherList().add(new CVoucher(34862, 40));
        getVoucherList().add(new CVoucher(14652, 40));
        getVoucherList().add(new CVoucher(40685, 40));
        getVoucherList().add(new CVoucher(92061, 40));
        getVoucherList().add(new CVoucher(70056, 50));
        getVoucherList().add(new CVoucher(41603, 50));
        getVoucherList().add(new CVoucher(80320, 50));
        getVoucherList().add(new CVoucher(40381, 50));
        getVoucherList().add(new CVoucher(44306, 50));
        getVoucherList().add(new CVoucher(10641, 50));
        getVoucherList().add(new CVoucher(70651, 50));
        getVoucherList().add(new CVoucher(80325, 50));
        getVoucherList().add(new CVoucher(50516, 50));
        getVoucherList().add(new CVoucher(10056, 50));
    }


    public static List <CVoucher> getVoucherList() {
        return voucherList;
    }

    public static void setVoucherList(List <CVoucher> voucherList) {
        CVoucherServices.voucherList = voucherList;
    }

    public static CVoucher findVoucherById (Integer maVoucher) {
        for (int i = 0 ; i < voucherList.size(); ++i) {
            if(voucherList.get(i).getMaVoucher().equals(maVoucher)){
                return voucherList.get(i);
            }
        }
        return null;
    }

}
