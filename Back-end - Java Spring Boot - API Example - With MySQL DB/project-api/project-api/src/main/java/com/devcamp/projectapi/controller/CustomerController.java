package com.devcamp.projectapi.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.devcamp.projectapi.interfacelist.AllErrorHandler;
import com.devcamp.projectapi.model.Customers;
import com.devcamp.projectapi.services.CustomersServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CustomerController implements AllErrorHandler {
    @Autowired
    CustomersServices customersServices;

    @GetMapping("/customers")
    public ResponseEntity<Object> getAllCustomer() {
        return new ResponseEntity<Object>(customersServices.findAll(), HttpStatus.OK);
    }

    @GetMapping("/customers/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable Integer id) {
        if (customersServices.findById(id).isPresent()) {
            return new ResponseEntity<Object>(customersServices.findById(id).get(), HttpStatus.OK);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Không tìm thấy customer theo Id yêu cầu");
        }
    }

    @GetMapping("/customers/filter/country")
    public ResponseEntity<Object> getCustomerNumberByCountry() {
        return new ResponseEntity<>(customersServices.getCustomerByCountry(), HttpStatus.OK);
    }

    @GetMapping ("testingLike/{string}")
    public List<Customers> getCustomerByCountryLike (@PathVariable String string) {
        return customersServices.findCustomerByCountryNameDesc(string);
    }

    @GetMapping ("testing2/{string}/{page}/{number}")
    public List<Customers> getCustomerByCityLike (@PathVariable String string,@PathVariable int page,@PathVariable int number) {
        return customersServices.findCustomerByCityName(string,PageRequest.of(page, number));
    }

    @GetMapping("testing3/{string}")
    public int updateCountry (@PathVariable String string) {
        return customersServices.updateCountry(string);
    }

    @PostMapping("/customers")
    public ResponseEntity<Object> createCustomer(@Valid @RequestBody Customers customers) {
        try {
            return new ResponseEntity<Object>(customersServices.save(customers), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause().getCause().getMessage(),HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/customers/{id}")
    public ResponseEntity<Object> updateCustomer(@PathVariable Integer id, @Valid @RequestBody Customers customers) {
        Optional<Customers> _customers = customersServices.findById(id);
        if (_customers.isPresent()) {
            _customers.get().setFirstName(customers.getFirstName());
            _customers.get().setLastName(customers.getLastName());
            _customers.get().setPhoneNumber(customers.getPhoneNumber());
            _customers.get().setAddress(customers.getAddress());
            _customers.get().setCity(customers.getCity());
            _customers.get().setState(customers.getState());
            _customers.get().setPostalCode(customers.getPostalCode());
            _customers.get().setCountry(customers.getCountry());
            _customers.get().setSalesRepEmployeeNumber(customers.getSalesRepEmployeeNumber());
            _customers.get().setCreditLimit(customers.getCreditLimit());
            try {
                return new ResponseEntity<Object>(customersServices.save(_customers.get()), HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified Customer: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/customers/{id}")
    public ResponseEntity<Object> deleteCustomer(@PathVariable Integer id){
        if(customersServices.findById(id).isPresent()){
            customersServices.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
