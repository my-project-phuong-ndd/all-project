package com.devcamp.projectapi.controller;

import java.util.Optional;

import javax.validation.Valid;

import com.devcamp.projectapi.interfacelist.AllErrorHandler;
import com.devcamp.projectapi.model.Employees;
import com.devcamp.projectapi.services.EmployeesServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class EmployeesController implements AllErrorHandler {
    @Autowired
    EmployeesServices employeesServices;

    @GetMapping("/employees")
    public ResponseEntity<Object> getAllEmployess() {
        return new ResponseEntity<Object>(employeesServices.findAll(), HttpStatus.OK);
    }

    @GetMapping("/employees/{id}")
    public ResponseEntity<Object> getEmployessById(@PathVariable Integer id) {
        if (employeesServices.findById(id).isPresent()) {
            return new ResponseEntity<Object>(employeesServices.findById(id).get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/employees")
    public ResponseEntity<Object> createEmployess(@Valid @RequestBody Employees employees) {
        try {
            return new ResponseEntity<Object>(employeesServices.save(employees), HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Employess: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/employees/{id}")
    public ResponseEntity<Object> updateEmployess(@PathVariable Integer id, @Valid @RequestBody Employees employees){
        Optional<Employees> _employess = employeesServices.findById(id);
        if(_employess.isPresent()){
            _employess.get().setFirstName(employees.getFirstName());
            _employess.get().setLastName(employees.getLastName());
            _employess.get().setExtension(employees.getExtension());
            _employess.get().setEmail(employees.getEmail());
            _employess.get().setOfficeCode(employees.getOfficeCode());
            _employess.get().setReportTo(employees.getReportTo());
            _employess.get().setJobTitle(employees.getJobTitle());
            try {
                return new ResponseEntity<Object>(employeesServices.save(_employess.get()), HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Employess: " + e.getCause().getCause().getMessage());
            }
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/employees/{id}")
    public ResponseEntity<Object> deleteEmployess(@PathVariable Integer id){
        if(employeesServices.findById(id).isPresent()){
            employeesServices.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
