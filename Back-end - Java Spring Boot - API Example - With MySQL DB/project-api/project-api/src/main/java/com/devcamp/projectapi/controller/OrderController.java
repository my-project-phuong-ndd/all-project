package com.devcamp.projectapi.controller;

import java.util.Optional;

import javax.validation.Valid;

import com.devcamp.projectapi.interfacelist.AllErrorHandler;
import com.devcamp.projectapi.model.Orders;
import com.devcamp.projectapi.services.CustomersServices;
import com.devcamp.projectapi.services.OrdersServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class OrderController implements AllErrorHandler{

    @Autowired
    OrdersServices ordersServices;

    @Autowired
    CustomersServices customersServices;

    @GetMapping("/orders")
    public ResponseEntity<Object> getAllOrders() {
        return new ResponseEntity<Object>(ordersServices.findAll(), HttpStatus.OK);
    }

    @GetMapping("/orders/{id}")
    public ResponseEntity<Object> getOrderById(@PathVariable Integer id) {
        if (ordersServices.findById(id).isPresent()) {
            return new ResponseEntity<Object>(ordersServices.findById(id).get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/orders/customer/{id}")
    public ResponseEntity<Object> getOrderByCustomerId(@PathVariable Integer id) {
        if (ordersServices.findByCustomersId(id).size() != 0) {
            return new ResponseEntity<Object>(ordersServices.findByCustomersId(id), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/orders")
    public ResponseEntity<Object> createOrder(@RequestParam (required = true,name = "customerId") Integer id, @Valid @RequestBody Orders orders) {
        if (customersServices.findById(id).isPresent()) {
            try {
                orders.setCustomers(customersServices.findById(id).get());
                return new ResponseEntity<Object>(ordersServices.save(orders), HttpStatus.CREATED);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Create specified Order: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/orders/{id}")
    public ResponseEntity<Object> updateOrder(@PathVariable Integer id,
            @Valid @RequestBody Orders orders) {
        Optional<Orders> _order = ordersServices.findById(id);
        if (_order.isPresent()) {
            _order.get().setOrderDate(orders.getOrderDate());
            _order.get().setRequiredDate(orders.getRequiredDate());
            _order.get().setShippedDate(orders.getShippedDate());
            _order.get().setStatus(orders.getStatus());
            _order.get().setComments(orders.getComments());
            try {
                return new ResponseEntity<Object>(ordersServices.save(_order.get()), HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified Order: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/orders/{id}")
    public ResponseEntity<Object> deleteOrder(@PathVariable Integer id) {
        if (ordersServices.findById(id).isPresent()) {
            ordersServices.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
