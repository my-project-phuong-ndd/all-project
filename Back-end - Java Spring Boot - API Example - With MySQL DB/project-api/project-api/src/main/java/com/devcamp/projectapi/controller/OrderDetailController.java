package com.devcamp.projectapi.controller;

import java.util.Optional;

import javax.validation.Valid;

import com.devcamp.projectapi.interfacelist.AllErrorHandler;
import com.devcamp.projectapi.model.OrderDetails;
import com.devcamp.projectapi.services.OrderDetailsServices;
import com.devcamp.projectapi.services.OrdersServices;
import com.devcamp.projectapi.services.ProductsServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class OrderDetailController implements AllErrorHandler {
    @Autowired
    OrderDetailsServices orderDetailsServices;

    @Autowired
    OrdersServices ordersServices;

    @Autowired
    ProductsServices productsServices;

    @GetMapping("/orderdetails")
    public ResponseEntity<Object> getAllOrderDetail() {
        return new ResponseEntity<Object>(orderDetailsServices.findAll(), HttpStatus.OK);
    }

    @GetMapping("/orderdetails/{id}")
    public ResponseEntity<Object> getOrderDetailById(@PathVariable Integer id) {
        if (orderDetailsServices.findById(id).isPresent()) {
            return new ResponseEntity<Object>(orderDetailsServices.findById(id).get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @GetMapping("/orderdetails/order/{id}")
    public ResponseEntity<Object> getOrderDetailByOrderId(@PathVariable Integer id) {
        if (orderDetailsServices.findByOrdersId(id).size() != 0) {
            return new ResponseEntity<Object>(orderDetailsServices.findByOrdersId(id), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/orderdetails")
    public ResponseEntity<Object> createOrderDetail(@RequestParam(required = true) Integer orderId,
            @RequestParam(required = true) Integer productId,
            @Valid @RequestBody OrderDetails orderDetails) {
        if (ordersServices.findById(orderId).isPresent() && productsServices.findById(productId).isPresent()) {
            try {
                orderDetails.setOrders(ordersServices.findById(orderId).get());
                orderDetails.setProducts(productsServices.findById(productId).get());
                return new ResponseEntity<Object>(orderDetailsServices.save(orderDetails), HttpStatus.CREATED);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Create specified OrderDetail: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<Object>("Không tìm thấy order và product để tạo orderdetail",
                    HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/orderdetails/{id}")
    public ResponseEntity<Object> updateOrderDetail(@PathVariable Integer id,
            @Valid @RequestBody OrderDetails orderDetails) {
        Optional<OrderDetails> _orderDetail = orderDetailsServices.findById(id);
        if (_orderDetail.isPresent()) {
            _orderDetail.get().setQuantityOrder(orderDetails.getQuantityOrder());
            _orderDetail.get().setPriceEach(orderDetails.getPriceEach());
            try {
                return new ResponseEntity<Object>(orderDetailsServices.save(_orderDetail.get()), HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified OrderDetail: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/orderdetails/{id}")
    public ResponseEntity<Object> deleteOrderDetail(@PathVariable Integer id) {
        if (orderDetailsServices.findById(id).isPresent()) {
            orderDetailsServices.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
