package com.devcamp.projectapi.controller;

import java.util.Optional;

import javax.validation.Valid;

import com.devcamp.projectapi.interfacelist.AllErrorHandler;
import com.devcamp.projectapi.model.Payments;
import com.devcamp.projectapi.services.CustomersServices;
import com.devcamp.projectapi.services.PaymentsServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class PaymentController implements AllErrorHandler {

    @Autowired
    PaymentsServices paymentsServices;

    @Autowired
    CustomersServices customersServices;

    @GetMapping("/payments")
    public ResponseEntity<Object> getAllPayments() {
        return new ResponseEntity<Object>(paymentsServices.findAll(), HttpStatus.OK);
    }

    @GetMapping("/payments/{id}")
    public ResponseEntity<Object> getPaymentById(@PathVariable Integer id) {
        if (paymentsServices.findById(id).isPresent()) {
            return new ResponseEntity<Object>(paymentsServices.findById(id).get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/payments/customer/{id}")
    public ResponseEntity<Object> getPaymentByCustomerId(@PathVariable Integer id) {
        if (paymentsServices.findByCustomersId(id).size() != 0) {
            return new ResponseEntity<Object>(paymentsServices.findByCustomersId(id), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/payments")
    public ResponseEntity<Object> createPayment(@RequestParam (required = true,name = "customerId") Integer id, @Valid @RequestBody Payments payments) {
        if (customersServices.findById(id).isPresent()) {
            try {
                payments.setCustomers(customersServices.findById(id).get());
                return new ResponseEntity<Object>(paymentsServices.save(payments), HttpStatus.CREATED);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Create specified Payment: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/payments/{id}")
    public ResponseEntity<Object> updatePayment(@PathVariable Integer id,
            @Valid @RequestBody Payments payments) {
        Optional<Payments> _payment = paymentsServices.findById(id);
        if (_payment.isPresent()) {
            _payment.get().setCheckNumber(payments.getCheckNumber());
            _payment.get().setPaymentDate(payments.getPaymentDate());
            _payment.get().setAmount(payments.getAmount());
            try {
                return new ResponseEntity<Object>(paymentsServices.save(_payment.get()), HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified Order: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/payments/{id}")
    public ResponseEntity<Object> deletePayment(@PathVariable Integer id) {
        if (paymentsServices.findById(id).isPresent()) {
            paymentsServices.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
