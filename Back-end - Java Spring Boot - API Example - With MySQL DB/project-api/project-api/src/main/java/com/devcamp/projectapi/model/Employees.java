package com.devcamp.projectapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table (name = "employees",uniqueConstraints = {@UniqueConstraint(name = "uk_email_employee",columnNames = "email")})
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class Employees {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;

    @Column (length = 50,nullable = false)
    private String firstName;
    
    @Column (length = 50,nullable = false)
    private String lastName;

    @Column (length = 50,nullable = false)
    private String extension;

    @Column (name="email",length = 50,nullable = false)
    private String email;

    @Column (name = "office_code",nullable = false)
    private int officeCode;

    @Column (name = "report_to",nullable = true)
    private int reportTo;

    @Column (length = 50,nullable = false)
    private String jobTitle;
}
