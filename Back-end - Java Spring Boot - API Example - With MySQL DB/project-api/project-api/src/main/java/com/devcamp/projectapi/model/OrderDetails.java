package com.devcamp.projectapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table (name = "order_details")
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class OrderDetails {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;
    
    @Column (name="quantity_order",nullable = false)
    private int quantityOrder;

    @Column (name = "price_each",nullable = false,columnDefinition = "Decimal(10,2)")
    private double priceEach;

    @ManyToOne (fetch = FetchType.LAZY)
    @JoinColumn (name = "product_id",nullable = false,foreignKey = @ForeignKey (name = "fk_order_details_product"))
    @JsonIgnore
    private Products products;

    @ManyToOne (fetch = FetchType.LAZY)
    @JoinColumn (name= "order_id",nullable = false,foreignKey = @ForeignKey (name = "order_details_orders"))
    @JsonIgnore
    private Orders orders;

    public Integer getProductId () {
        return getProducts().getId();
    }

    public String getProductName () {
        return getProducts().getProductName();
    }

    public Integer getOrderId () {
        return getOrders().getId();
    }
}