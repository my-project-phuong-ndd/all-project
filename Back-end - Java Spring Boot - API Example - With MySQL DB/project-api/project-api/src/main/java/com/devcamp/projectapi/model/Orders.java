package com.devcamp.projectapi.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table (name = "orders")
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class Orders {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;

    @Temporal (TemporalType.TIMESTAMP)
    @Column (nullable = false)
    @JsonFormat (pattern = "dd-MM-yyyy")
    private Date orderDate;

    @Temporal (TemporalType.TIMESTAMP)
    @Column (nullable = false)
    @JsonFormat (pattern = "dd-MM-yyyy")
    private Date requiredDate;

    @Temporal (TemporalType.TIMESTAMP)
    @Column (nullable = true)
    @JsonFormat (pattern = "dd-MM-yyyy")
    private Date shippedDate;

    @Column (length = 50)
    private String status;

    private String comments;

    @OneToMany (fetch = FetchType.LAZY,cascade = CascadeType.ALL,mappedBy = "orders")
    @JsonIgnore
    private List <OrderDetails> orderDetails;

    @ManyToOne (fetch = FetchType.LAZY)
    @JoinColumn (name="customer_id",foreignKey = @ForeignKey (name="fk_orders_customers"),nullable = false)
    @JsonIgnore
    private Customers customers;

    public Integer getCustomerId () {
        return getCustomers().getId();
    }

    public String getCustomerName () {
        return String.format("%s %s", getCustomers().getFirstName(), getCustomers().getLastName());
    }
}