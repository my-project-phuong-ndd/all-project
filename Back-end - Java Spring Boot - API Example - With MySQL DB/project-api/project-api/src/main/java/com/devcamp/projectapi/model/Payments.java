package com.devcamp.projectapi.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table (name = "payments")
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class Payments {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;

    @Column (length = 50,nullable = false)
    private String checkNumber;

    @Temporal(TemporalType.TIMESTAMP)
    @Column (nullable = false)
    @JsonFormat (pattern = "dd-MM-yyyy")
    private Date paymentDate;

    @Column (precision = 10,scale = 2,nullable = false)
    private BigDecimal amount;

    @ManyToOne (fetch = FetchType.LAZY)
    @JoinColumn (name = "customer_id",foreignKey = @ForeignKey (name = "fk_payments_ibfk_1"),nullable = false)
    @JsonIgnore
    private Customers customers;

    public Integer getCustomerId() {
        return getCustomers().getId();
    }

    public String getCustomerFullName() {
        return String.format("%s %s", getCustomers().getFirstName(), getCustomers().getLastName());
    }

}