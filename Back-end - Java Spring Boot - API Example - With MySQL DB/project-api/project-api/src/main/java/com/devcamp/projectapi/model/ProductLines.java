package com.devcamp.projectapi.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table (name = "product_lines",uniqueConstraints = {@UniqueConstraint (name = "product_line",columnNames = "product_line")})
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class ProductLines {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;

    @Column (length = 50,name = "product_line",nullable = false)
    private String productLine;

    @Column (length = 2500,nullable = false)
    private String description;

    @OneToMany (fetch = FetchType.LAZY,mappedBy = "productLines",cascade = CascadeType.ALL)
    @JsonIgnore
    private List <Products> products;
}