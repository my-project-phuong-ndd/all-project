package com.devcamp.projectapi.repository;

import com.devcamp.projectapi.model.Customers;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomersRepository extends JpaRepository <Customers,Integer>  {
}
