package com.devcamp.projectapi.repository;

import com.devcamp.projectapi.model.Employees;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeesRepository extends JpaRepository <Employees,Integer>  {
    
}
