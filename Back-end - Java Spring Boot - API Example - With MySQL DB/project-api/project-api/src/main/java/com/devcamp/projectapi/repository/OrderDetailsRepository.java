package com.devcamp.projectapi.repository;

import com.devcamp.projectapi.model.OrderDetails;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderDetailsRepository extends JpaRepository <OrderDetails,Integer>  {

}
