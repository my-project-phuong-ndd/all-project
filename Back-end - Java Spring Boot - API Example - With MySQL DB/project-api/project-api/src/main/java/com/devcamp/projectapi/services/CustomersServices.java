package com.devcamp.projectapi.services;

import java.util.List;

import com.devcamp.projectapi.model.Customers;
import com.devcamp.projectapi.repository.CustomersRepository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface CustomersServices extends CustomersRepository {
    @Query(value = "SELECT * FROM customers  WHERE country like ?1 ORDER BY country DESC", nativeQuery = true)
	List<Customers> findCustomerByCountryNameDesc(String name1);

    @Query(value = "SELECT * FROM `customers` WHERE (first_name LIKE :para1 AND last_name LIKE :para2);", nativeQuery = true)
	List<Customers> findCustomerByCustomerName(@Param("para1") String name1,@Param("para2") String name2);

    @Query(value = "SELECT * FROM `customers` WHERE city LIKE ?1", nativeQuery = true)
	List<Customers> findCustomerByCityName(String name1,Pageable pageable);

    @Query (value = "SELECT country,COUNT(country) AS \"number\" FROM `customers` GROUP BY country ORDER BY `Number` DESC;",nativeQuery = true)
    List <Object> getCustomerByCountry ();

    @Query(value = "SELECT * FROM `customers` WHERE city LIKE :name1 LIMIT :page,:number", nativeQuery = true)
	List<Customers> findCustomerByCityName(@Param("name1") String name1,@Param("page") int page,@Param("number") int number);

    @Query(value = "SELECT * FROM `customers` WHERE country LIKE ?1 ORDER BY first_name DESC", nativeQuery = true)
	List<Customers> findCustomerByCityNameWithOrder(String name1,Pageable pageable);

    @Query(value = "SELECT * FROM `customers` WHERE country LIKE :name1 ORDER BY first_name DESC LIMIT :page,:number", nativeQuery = true)
	List<Customers> findCustomerByCityNameWithOrder(@Param("name1") String name1,@Param("page") int page,@Param("number") int number);

    @Transactional
    @Modifying
    @Query(value = "UPDATE customers SET country = :name WHERE country IS NULL ", nativeQuery = true)
    int updateCountry(@Param("name") String name);
}
