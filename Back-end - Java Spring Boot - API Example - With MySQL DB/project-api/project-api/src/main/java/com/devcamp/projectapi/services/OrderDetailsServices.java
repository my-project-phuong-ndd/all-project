package com.devcamp.projectapi.services;

import java.util.List;

import com.devcamp.projectapi.model.OrderDetails;
import com.devcamp.projectapi.repository.OrderDetailsRepository;

public interface OrderDetailsServices extends OrderDetailsRepository {
    List <OrderDetails> findByOrdersId (Integer id);
}
