package com.devcamp.projectapi.services;

import java.util.List;

import com.devcamp.projectapi.model.Orders;
import com.devcamp.projectapi.repository.OrdersRepository;

public interface OrdersServices extends OrdersRepository {
    List <Orders> findByCustomersId (Integer id);
}
