package com.devcamp.projectapi.services;

import java.util.List;

import com.devcamp.projectapi.model.Payments;
import com.devcamp.projectapi.repository.PaymentsRepository;

public interface PaymentsServices extends PaymentsRepository {
    List <Payments> findByCustomersId (Integer id);
}
