function checkDataValid (paramElementCheck,paramAllElement = "",paramObjectRequest = "") {
  paramElementCheck = !Array.isArray(paramElementCheck) ? [paramElementCheck] : paramElementCheck;
  let vErrorDetail = {
    required : (value,title) => 
              !value ? `${title} chưa được điền hoặc chọn` : "",

    // p{L} : Các chữ Latin , \s:Khoảng trắng , ^[] , +
    name : (value,title) => (!/^([\p{L}\s]+[\p{L}]\s?)*$/u.test(value)) ? 
           `${title} nhập vào không hợp lệ (Chỉ gồm chữ, không có số và ký tự đặc biệt)` : "",

    email : (value,title) => 
            (!/^(\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,6})+)*$/.test(value)) ? 
            `${title} nhập vào không hợp lệ` : "",
        

    // số đầu là 0 - có thể thay 0 bằng 84 hoặc +84 , số thứ 2 là một trong các số 35789, tổng là 10 số 
    phone : (value,title) => (!/^((\+*84|[0]{1})+[35789]{1}[0-9]{8})*$/.test(value)) ? 
            `${title} nhập vào không hợp lệ` : "",

    minChar : (number,value,title) => (value && value.length < number) ? 
          `${title} phải có ít nhất ${number} ký tự` : "",

    minValue : (number,value,title) => (value && value < number) ? 
          `${title} phải lớn hơn hoặc bằng ${number}` : "",

    maxChar : (number,value,title) => (value && value.length > number) ? 
          `${title} chỉ có nhiều nhất ${number} ký tự` : "",

    maxValue : (number,value,title) => (value && value > number) ? 
          `${title} phải nhỏ hơn hoặc bằng ${number}` : "",

    // Chữ đầu tiên là chữ, các ký tự tiếp theo là chữ , số hoặc .-_ tối thiểu 6 ký tự
    username : (value,title) => (value && !/^([a-zA-Z]{1}[a-zA-Z.-_\d]{5,})*$/.test(value)) ? `${title} không hợp lệ` : "",

    // Hoặc là 0 hoặc lớn hơn 0, là số (Không được có số 0 ở đầu)
    price : (value,title) => (value && !/^(^0$|[1-9]{1}[\d]*)*$/.test(value)) ? `${title} không phải là số, không có số 0 ở đầu hoặc giá phải bằng 0` : "",

    // Phải nhỏ hơn hoặc bằng price
    discountPrice : (originalValue,discountValue,title) => (discountValue && Number(discountValue) > Number(originalValue)) ? 
    `${title} phải nhỏ hơn hoặc bằng giá gốc` : "",

    number : (value,title) => (!/^[\d]*$/.test(value)) ? `${title} phải là số` : "",
    
    image : 4,

    radiocheck : 5,

    checkboxcheck : 1,

    website : (value,title) => (value && !/^(\w+([\.-]?\w+)*(\.\w{2,4}))*$/.test(value)) ? `${title} không hợp lệ` : "",

    // -90.0000 tới 90.0000
    geoLat : (value,title) => (value && !/^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,6})?))$/.test(value)) ? `${title} không hợp lệ` : "",
    // -180.0000 tới 180.0000
    geoLng : (value,title) => (value && !/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,6})?))$/.test(value)) ? `${title} không hợp lệ` : "",

    // XXXXX hoặc XXXXX-XXXX
    zipcodeUS : (value,title) => (value && !/^(\d{5}-\d{4}|\d{5})*$/.test(value)) ? `${title} không hợp lệ` : "",

    // {8,} : ít nhất 8 ký tự ;
    // (?=.*[a-z]) : ít nhất 1 ký tự thường ;
    // (?=.*[A-Z]) : ít nhất 1 ký tự hoa ;
    // (?=.*\d) : ít nhất 1 số ;
    // (?=.*[@$!%*?&]) : ít nhất 1 ký tự đặc biệt
    password : (value,title) => (value && !/^((?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,})*$/.test(value)) ? `${title} không hợp lệ` : "",

    recheckpass : (value,recheckvalue,title) => (recheckvalue && value != recheckvalue) ? 
                  `${title} và password phải trùng khớp nhau` : "", 

    // Updating
  };

  var vErrorCheck = "";

  paramElementCheck.forEach((pValue) => {
    let bCheckType = pValue.dataset.checktype ? pValue.dataset.checktype.split("|") : $(pValue).data("dataChecktype").split("|");
    let bTitle = pValue.title;
    let bValueCheck = paramObjectRequest ? paramObjectRequest[pValue.dataset.property ? pValue.dataset.property : $(pValue).data("dataProperty")] : pValue.value.trim();
    let bError = "";
    let bCheckIndex = 0;

    while (!bError && bCheckIndex < bCheckType.length) {
      switch (true) {

        case bCheckType[bCheckIndex].includes(":") : {
          let bTypeWithNumber = bCheckType[bCheckIndex].split(":");
          bError = vErrorDetail[bTypeWithNumber[0]](bTypeWithNumber[1],bValueCheck,bTitle);
          vErrorCheck += bError ? bError + '\n' : "";
        };
        break;

        case bCheckType[bCheckIndex] == "recheckpass" : {
          let bPasswordValue = paramAllElement.find(pValue => /^(?!.*\brecheckpass\b.*).*\bpassword\b.*$/.test(pValue.dataset.checktype) || /^(?!.*\brecheckpass\b.*).*\bpassword\b.*$/.test($(pValue).data("dataCheckType"))).value.trim();
          bError = vErrorDetail[bCheckType[bCheckIndex]](bPasswordValue,bValueCheck,bTitle);
          vErrorCheck += bError ? bError + `\n` : "";
        };
        break;

        case bCheckType[bCheckIndex] == "discountPrice" : {
          let bOriginal  = paramAllElement.find(pValue => /^(?!.*\bdiscountPrice\b.*).*\bprice\b.*$/.test(pValue.dataset.checktype) || /^(?!.*\bdiscountPrice\b.*).*\bprice\b.*$/.test($(pValue).data("dataCheckType"))).value.trim();
          bError = vErrorDetail[bCheckType[bCheckIndex]](bOriginal,bValueCheck,bTitle);
          vErrorCheck += bError ? bError + `\n` : "";
        };
        break;

        default : {
          bError = vErrorDetail[bCheckType[bCheckIndex]](bValueCheck,bTitle);
          vErrorCheck += bError ? bError + `\n` : "";
        }
      }
      ++bCheckIndex;
    }
  })

  return vErrorCheck;
}

// Database = Array , type = "create" or "edit"
function checkUniqueValue (paramValue,paramDatabase,paramType = "create",indexValue = 0) {

}


// Check data input từng ô (Có tương thích với select2)
function dynamicCheckDataForm (paramElement,paramAllElement,paramObjectRequest = "") {
  var vResult = "";
  paramElement.parentElement.querySelectorAll(".form-control,.select2-selection").forEach(element => element.classList.remove("is-invalid","is-valid"));
  let bValueCheck = paramObjectRequest ? paramObjectRequest[paramElement.dataset.property ? paramElement.dataset.property : $(paramElement).data("dataProperty")] : paramElement.value.trim();
  vResult = checkDataValid (paramElement,paramAllElement,paramObjectRequest);
    if (vResult) {
      paramElement.parentElement.getElementsByClassName("invalid-feedback")[0].innerHTML = vResult;
      paramElement.parentElement.querySelectorAll(".form-control,.select2-selection").forEach(element => element.classList.add("is-invalid"));      
      return;
    } else if (bValueCheck) {
        paramElement.classList.add("is-valid");
        paramElement.parentElement.querySelectorAll(".form-control,.select2-selection").forEach(element => element.classList.add("is-valid"))
    };      
}


function loadDataToSelect (paramArray) {

}


// Testing đệ quy (recursive) để cố định toàn bộ Object (Seal hoặc freeze) tránh sửa dữ liệu
function modifyAllObject (paramAction) {
  // paramAction là "freeze" hoặc "seal" 
  // paramArrayObj = [paramObj1,paramObj2...]
  
  try {
    if(!(paramAction == "freeze" || paramAction == "seal")) {
      throw (false,"Đây không phải tính năng freeze hay seal")
    }
  
    if(arguments.length <= 1) {
      throw (false,"Hàm Freeze Seal không có dữ liệu đầu vào");
    }
  }

  catch (err) {
    console.assert(false,err);
    return;
  }

    for (let bIndex = 1; bIndex < arguments.length; bIndex++) {
      for (let bIndexObj in arguments[bIndex]) {
        if (typeof arguments[bIndex][bIndexObj] == "object") {
          modifyAllObject (paramAction,arguments[bIndex][bIndexObj]);
        }
        Object[paramAction] (arguments[bIndex]);
      }
    }
}

function addDataToTable () {
    
}


// Closure ứng dụng
function storedElementHTML (paramAllSelector) {

  paramAllSelector = !Array.isArray(paramAllSelector) ? [paramAllSelector] : paramAllSelector;

  const vAllAttributeElement = paramAllSelector.map((element) => {
    var bAllAttributeObj = {};
    element.getAttributeNames().forEach((attributeName) =>
      bAllAttributeObj[attributeName] = element.getAttribute(attributeName));
    return bAllAttributeObj;
  });

  const vGetAttributeElement = {

    // Arrow function , bỏ từ function và thay bằng dấu =>
    // Nếu chỉ có 1 hành động duy nhất thì bỏ luôn scope {} và thay bằng hành động , đồng thời bỏ chữ return
    // Nếu chỉ có 1 tham số thì không cần dấu () mà ghi thẳng param ra
    getAllElementJquery : () => $(paramAllSelector),
    getAllElement : () => paramAllSelector,
    getElementByIndex : index => paramAllSelector[index],
    // chỉ tính các attribute trên dom (Ko có các attribute trong data jquery)
    getAttributeValue : (index,attribute) => vAllAttributeElement[index][attribute],

    // Đổi từ dataset sang data Jquery và khóa lại
    changeDatasetToDataJqueryAndSeal : () => changeDatasetToDataJqueryAndSeal(),

    // Find index
    findIndexByAttributeOrData : (typeAttribute,typeSearch,attribute,value) => findIndex(typeAttribute,typeSearch,attribute,value),
    findIndexElementByNode : element => paramAllSelector.findIndex((allElement) => allElement.isSameNode(element) == true),

    // Reset Element về mặc định
    resetElement : element => resetElement(element)
  }

  function resetElement (paramElement) {
    if (paramElement !== undefined && paramElement.nodeType != 1) {
      throw("ResetElement : Giá trị nhập vào là phải Node")
    }
    paramElement = paramElement === undefined ? paramAllSelector : paramElement;
    paramElement = !Array.isArray(paramElement) ? [paramElement] : paramElement;
    
    paramElement.forEach(function(pValue){
      var vIndex = vGetAttributeElement.findIndexElementByNode(pValue);
      for(let bIndex in vAllAttributeElement[vIndex]) {
        if (pValue.getAttribute(bIndex) != vAllAttributeElement[vIndex][bIndex]) {
          pValue.setAttribute(bIndex,vAllAttributeElement[vIndex][bIndex]);
        }
      }
    })
    
  }

  function findIndex (pTypeAttribute,pTypeSearch,pAttribute,pValue) {
    var vFindIndex = {
      attribute : {
        exactly: (pAttribute,value) => vAllAttributeElement.findIndex(allAttribute => allAttribute[pAttribute] == value),
        contains: (pAttribute,value) => vAllAttributeElement.findIndex(allAttribute => allAttribute[pAttribute].includes(value)),
        regexp : (pAttribute,value) => vAllAttributeElement.findIndex(allAttribute => value.test(allAttribute[pAttribute]))
      },
      data : {
        exactly: (pAttribute,value) => paramAllSelector.findIndex(allElement => $(allElement).data(pAttribute) == value),
        contains: (pAttribute,value) => paramAllSelector.findIndex(allElement => $(allElement).data(pAttribute).includes(value)),
        regexp : (pAttribute,value) => paramAllSelector.findIndex(allElement => value.test($(allElement).data(pAttribute)))
      }
    }

    if (vFindIndex[pTypeAttribute] === undefined) {
      throw (`Find Index : Type Attribute phải là attribute hoặc data`)
    } else if (vFindIndex[pTypeAttribute][pTypeSearch] === undefined) {
      throw (`Find Index : Type Search phải là exactly,contains hoặc regexp`)
    }
    return vFindIndex[pTypeAttribute][pTypeSearch](pAttribute,pValue)

  }

  function changeDatasetToDataJqueryAndSeal () {
  $(paramAllSelector).each(function(pIndex) {
    for(let bDataset in this.dataset) {
      delete vAllAttributeElement[pIndex][`data-${bDataset}`];
      $(this).data(`data-${bDataset}`,this.dataset[bDataset])
             .removeData(bDataset)
             .removeAttr(`data-${bDataset}`);
      var bData = bDataset.slice(0,1).toUpperCase() + bDataset.slice(1).toLowerCase();

      Object.defineProperties($(this).data(),{
        [`data${bData}`] : {
          writable : false,
          configurable : false,
        }
      })
    }
  })
}
  
  return vGetAttributeElement;

}