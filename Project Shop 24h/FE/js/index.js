const $$ = document.querySelectorAll.bind(document);
const __ = document.querySelector.bind(document);

let slide_index = 0
let slide_play = true
let slides = document.querySelectorAll('.slide')

hideAllSlide = () => {
    slides.forEach(e => {
        e.classList.remove('active')
    })
}

showSlide = () => {
    hideAllSlide()
    slides[slide_index].classList.add('active')
}

nextSlide = () => slide_index = slide_index + 1 === slides.length ? 0 : slide_index + 1

prevSlide = () => slide_index = slide_index - 1 < 0 ? slides.length - 1 : slide_index - 1

// pause slide when hover slider

document.querySelector('.slider').addEventListener('mouseover', () => slide_play = false)

// enable slide when mouse leave out slider
document.querySelector('.slider').addEventListener('mouseleave', () => slide_play = true)

// slider controll

document.querySelector('.slide-next').addEventListener('click', () => {
    nextSlide()
    showSlide()
})

document.querySelector('.slide-prev').addEventListener('click', () => {
    prevSlide()
    showSlide()
})

showSlide()

// setInterval(() => {
//     if (!slide_play) return
//     nextSlide()
//     showSlide()
// }, 3000);

// render products

let products = [
    {
        id : 1,
        name: 'Iphone 11',
        image1: './images/iphone-11-1.png',
        image2: './images/iphone-11-2.jpg',
        old_price: '400',
        curr_price: '300'
    },
    {
        id : 2,
        name: 'Iphone 11 Pro',
        image1: './images/iphone-11-pro-1.jpg',
        image2: './images/iphone-11-pro-2.jpg',
        old_price: '400',
        curr_price: '300'
    },
    {
        id : 3,
        name: 'Iphone 11 Pro Max',
        image1: './images/iphone-11-pro-max-1.jpg',
        image2: './images/iphone-11-pro-max-2.webp',
        old_price: '400',
        curr_price: '300'
    },
    {
        id : 4,
        name: 'Iphone 12 Mini',
        image1: './images/iphone-12-mini-1.jpg',
        image2: './images/iphone-12-mini-2.png',
        old_price: '400',
        curr_price: '300'
    },
    {
        id : 5,
        name: 'Iphone 12',
        image1: './images/iphone-12-1.webp',
        image2: './images/iphone-12-2.png',
        old_price: '400',
        curr_price: '300'
    },
    {
        id : 6,
        name: 'Iphone 12 Pro',
        image1: './images/iphone-12-pro-1.jpg',
        image2: './images/iphone-12-pro-2.jpg',
        old_price: '400',
        curr_price: '300'
    },
    {
        id : 7,
        name: 'Iphone 12 Pro Max',
        image1: './images/iphone-11-pro-max-1.jpg',
        image2: './images/iphone-11-pro-max-2.webp',
        old_price: '400',
        curr_price: '300'
    },
    {
        id : 8,
        name: 'Iphone 13 Mini',
        image1: './images/iphone-12-mini-1.jpg',
        image2: './images/iphone-12-mini-2.png',
        old_price: '400',
        curr_price: '300'
    },
    {
        id : 9,
        name: 'Iphone 13',
        image1: './images/iphone-12-1.webp',
        image2: './images/iphone-12-2.png',
        old_price: '400',
        curr_price: '300'
    },
    {
        id : 10,
        name: 'Iphone 13 Pro',
        image1: './images/iphone-12-pro-1.jpg',
        image2: './images/iphone-12-pro-2.jpg',
        old_price: '400',
        curr_price: '300'
    },
    {
        id : 11,
        name: 'Iphone 13 Pro Max',
        image1: './images/iphone-11-pro-max-1.jpg',
        image2: './images/iphone-11-pro-max-2.webp',
        old_price: '400',
        curr_price: '300'
    },
]

updateCart();

let product_list = document.querySelector('#latest-products')
let best_product_list = document.querySelector('#best-products')

products.forEach(e => {
    let prod = `
        <div class="col-3 col-md-6 col-sm-12">
            <div class="product-card">
                <div class="product-card-img">
                    <img src="${e.image1}" alt="">
                    <img src="${e.image2}" alt="">
                </div>
                <div class="product-card-info">
                    <div class="product-btn">
                        <button class="btn-flat btn-hover btn-shop-now">Xem chi tiết</button>
                        <button class="btn-flat btn-hover btn-cart-add" data-id="${e.name}">
                            <i class='bx bxs-cart-add'></i>Thêm vào giỏ 
                        </button>
                    </div>
                    <div class="product-card-name">
                        ${e.name}
                    </div>
                    <div class="product-card-price">
                        <span><del>${e.old_price}$</del></span>
                        <span class="curr-price">${e.curr_price}$</span>
                    </div>
                </div>
            </div>
        </div>
    `

    product_list.insertAdjacentHTML("beforeend", prod)
    best_product_list.insertAdjacentHTML("afterbegin", prod)
})



$$(".btn-cart-add").forEach(function(e){
    e.onclick = function() {
        !sessionStorage.getItem("last-cart-info") ? sessionStorage.setItem("last-cart-info",JSON.stringify([])) : "";

        var vId = this.dataset.id;
        var vLastCart = JSON.parse(sessionStorage.getItem("last-cart-info"));
        var vIndexItem = vLastCart.findIndex(value => value[0] == vId)
        vIndexItem == -1 ? vLastCart.push([vId,1]) : ++vLastCart[vIndexItem][1] ;
        sessionStorage.setItem("last-cart-info",JSON.stringify(vLastCart));
        updateCart();
    }
})

function updateCart() {
    __("#cart-total").innerHTML = !JSON.parse(sessionStorage.getItem("last-cart-info")) ? 0 : JSON.parse(sessionStorage.getItem("last-cart-info")).length; 
}


