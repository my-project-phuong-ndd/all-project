// Region 1 : Biến toàn cục , closure 

const $$ = document.querySelectorAll.bind(document);
const __ = document.querySelector.bind(document);
const g_NUM_PRODUCT_PER_PAGE = 3;

let products = [
    {
        id : 1,
        name: 'Iphone 11',
        code : "IP11-N",
        image1: './images/iphone-11-1.png',
        image2: './images/iphone-11-2.jpg',
        old_price: '400',
        curr_price: '300'
    },
    {
        id : 2,
        name: 'Iphone 11 Pro',
        code : "IP11-P",
        image1: './images/iphone-11-pro-1.jpg',
        image2: './images/iphone-11-pro-2.jpg',
        old_price: '400',
        curr_price: '330'
    },
    {
        id : 3,
        name: 'Iphone 11 Pro Max',
        code : "IP11-PM",
        image1: './images/iphone-11-pro-max-1.jpg',
        image2: './images/iphone-11-pro-max-2.webp',
        old_price: '400',
        curr_price: '360'
    },
    {
        id : 4,
        name: 'Iphone 12 Mini',
        code : "IP12-M",
        image1: './images/iphone-12-mini-1.jpg',
        image2: './images/iphone-12-mini-2.png',
        old_price: '400',
        curr_price: '400'
    },
    {
        id : 5,
        name: 'Iphone 12',
        code : "IP12-N",
        image1: './images/iphone-12-1.webp',
        image2: './images/iphone-12-2.png',
        old_price: '400',
        curr_price: '420'
    },
    {
        id : 6,
        name: 'Iphone 12 Pro',
        code : "IP12-P",
        image1: './images/iphone-12-pro-1.jpg',
        image2: './images/iphone-12-pro-2.jpg',
        old_price: '400',
        curr_price: '450'
    },
    {
        id : 7,
        name: 'Iphone 12 Pro Max',
        code : "IP12-PM",
        image1: './images/iphone-11-pro-max-1.jpg',
        image2: './images/iphone-11-pro-max-2.webp',
        old_price: '400',
        curr_price: '480'
    },
    {
        id : 8,
        name: 'Iphone 13 Mini',
        code : "IP13-M",
        image1: './images/iphone-12-mini-1.jpg',
        image2: './images/iphone-12-mini-2.png',
        old_price: '400',
        curr_price: '700'
    },
    {
        id : 9,
        name: 'Iphone 13',
        code : "IP13-N",
        image1: './images/iphone-12-1.webp',
        image2: './images/iphone-12-2.png',
        old_price: '400',
        curr_price: '750'
    },
    {
        id : 10,
        name: 'Iphone 13 Pro',
        code : "IP13-P",
        image1: './images/iphone-12-pro-1.jpg',
        image2: './images/iphone-12-pro-2.jpg',
        old_price: '400',
        curr_price: '800'
    },
    {
        id : 11,
        name: 'Iphone 13 Pro Max',
        code : "IP13-PM",
        image1: './images/iphone-11-pro-max-1.jpg',
        image2: './images/iphone-11-pro-max-2.webp',
        old_price: '400',
        curr_price: '850'
    },
]

let tempProduct = products;

// Region 2 : Các event handler setup khởi điểm

// Load trang ban đầu
onPageLoading()

// Setup CSS
__('#filter-toggle').addEventListener('click', () => __('#filter-col').classList.toggle('active'))

__('#filter-close').addEventListener('click', () => __('#filter-col').classList.toggle('active'))

// Setup Event Handler cho nút chuyển trang 
__("#paging-product ul li:last-child a").onclick= function () {
    __("#paging-product li a.active").closest("li").nextElementSibling.firstElementChild.click();
}

__("#paging-product ul li:first-child a").onclick= function () {
    __("#paging-product li a.active").closest("li").previousElementSibling.firstElementChild.click();
}


// Set sự kiện filter cho các nút ở cột filter
$$("#filter-col input").forEach(function(e){
    e.onchange = function() {
        filterProduct()
    }
})

// Add sự kiện cho nút reset filter 
__("#reset-filter").onclick = function(){
    $$("#filter-col input[type=checkbox]").forEach(function(e){
        e.checked=false;
    });

    // Trigger onchange 1 nút để tái filter lại;
    __("#filter-col input:first-child").onchange();
}

// Get Param từ trang index và filter lại sản phẩm
let params = (new URL(document.location)).searchParams;
    if (params.get("brand")) {
        __(`#filter-by-version input[value=${params.get("brand")}]`).checked = true;
        __(`#filter-by-version input[value=${params.get("brand")}]`).onchange();
    };

    if (params.get("type")){
        __(`#filter-by-type input[value=${params.get("type")}]`).checked = true;
        __(`#filter-by-type input[value=${params.get("type")}]`).onchange();
    }

// Region 3 : Function cho Event 

function onPageLoading () {
    // Cập nhật trạng thái đơn hàng từ Session Storage
    updateCart();

    // Load Trang ban đầu và các yếu tố liên quan (Nút Paging , Event Add To Cart)
    loadProductToPage(tempProduct,g_NUM_PRODUCT_PER_PAGE)
}


// Load sản phẩm và các yếu tố liên quan ra trang
function loadProductToPage (product,numProductPerPage) {
    // Load sản phẩm ra trang
    renderProducts(product);

    // Tạo thanh Paging (Thanh số trang) dựa vào product và số trang mỗi page
    renderPaging(numProductPerPage);

    // Thêm event Add to Cart vào sản phẩm
    addEventAddToCart();

    // Thêm event vào các nút số trang Paging
    addEventToPaging(numProductPerPage);
}


function filterProduct () {
    // Gán biến tạm bằng sản phẩm tổng
    tempProduct = products;

    // Lọc sản phẩm qua version
    tempProduct = filterByVersion(tempProduct);

    // Lọc sản phẩm qua giá
    tempProduct = filterByPrice(tempProduct);

    // Lọc sản phẩm qua loại
    tempProduct = filterByType(tempProduct);

    // Load lại các sản phẩm đã lọc và load lại các yếu tố liên quan
    loadProductToPage(tempProduct,g_NUM_PRODUCT_PER_PAGE);

    // Lọc ngược bằng cách loại bỏ những phần tử không được chọn
    function filterByVersion(product) {
        var vArrUnchecked = $$("#filter-by-version input:not(:checked)");
        if (vArrUnchecked.length == $$("#filter-by-version input").length) {
            return product;
        } else {
            vArrUnchecked.forEach(function(e){
                product = product.filter(product => !(product.code).startsWith(e.value));
            })
        }
        return product;
    }

    function filterByPrice(product) {

        var vArrPrice = $$("#filter-by-price input");

        // Nếu giá Min hay Max = 0 thì sẽ không lọc
        var vPriceMin = vArrPrice[0].value == 0 ? -Infinity : Number(vArrPrice[0].value);
        var vPriceMax = vArrPrice[1].value == 0 ? +Infinity : Number(vArrPrice[1].value);

        // Lọc theo giá Min và Max
        product = product.filter(product =>  product.curr_price >= vPriceMin && product.curr_price <= vPriceMax);

        return product;
    }

    // Lọc ngược bằng cách loại bỏ những phần tử không được chọn
    function filterByType(product) {
        var vArrUnchecked = $$("#filter-by-type input:not(:checked)");
        if (vArrUnchecked.length == $$("#filter-by-type input").length) {
            return product;
        } else {
            vArrUnchecked.forEach(function(e){
                product = product.filter(product => !(product.code).endsWith(e.value));
            })
        }
        return product;
    }
}


// Region 4 : Function chung 

function renderProducts (product) {
    // Trước khi render sẽ xoá sạch product cũ
    __("#products").innerHTML = "";

    // Gán các value cần thiết vào HTML rồi parse HTML vào vị trí phù hợp
    product.forEach(e => {
        let prod = `
            <div class="col-4 col-md-6 col-sm-12">
                <div class="product-card">
                    <div class="product-card-img">
                        <img src="${e.image1}" alt="">
                        <img src="${e.image2}" alt="">
                    </div>
                    <div class="product-card-info">
                        <div class="product-btn">
                            <a href="./product-detail.html" class="btn-flat btn-hover btn-shop-now">Xem chi tiết</a>
                            <button class="btn-flat btn-hover btn-cart-add" data-id="${e.code}">
                                <i class='bx bxs-cart-add'></i>Thêm vào giỏ
                            </button>
                        </div>
                        <div class="product-card-name">
                            ${e.name}
                        </div>
                        <div class="product-card-price">
                            <span><del>${e.old_price}$</del></span>
                            <span class="curr-price">${e.curr_price}$</span>
                        </div>
                    </div>
                </div>
            </div>`

        __("#products").insertAdjacentHTML("beforeend", prod)
    })
}

// Số đỏ trên cart = Số sản phẩm add vào giỏ
function updateCart() {
    __("#cart-total").innerHTML = !JSON.parse(sessionStorage.getItem("last-cart-info")) ? 0 : JSON.parse(sessionStorage.getItem("last-cart-info")).length; 
}

// Gắn event Add to Cart vào nút giỏ hàng
function addEventAddToCart() {
    $$(".btn-cart-add").forEach(function(e){
        e.onclick = function() {
            // Nếu Session = null thì gán session = array rỗng
            !sessionStorage.getItem("last-cart-info") ? sessionStorage.setItem("last-cart-info",JSON.stringify([])) : "";
    
            // Lấy data-id property của nút
            var vId = this.dataset.id;

            var vLastCart = JSON.parse(sessionStorage.getItem("last-cart-info"));

            // Tìm vị trí của ID sản phẩm trong session, nếu có thì +1 vào , nếu không thì tạo mới và gắn value =1
            var vIndexItem = vLastCart.findIndex(value => value[0] == vId);
            vIndexItem == -1 ? vLastCart.push([vId,1]) : ++vLastCart[vIndexItem][1];

            // Cập nhật lại SessionStorage
            sessionStorage.setItem("last-cart-info",JSON.stringify(vLastCart));

            // Sau khi gán thì cập nhật trạng thái của cart
            updateCart();
        }
    })
}


function renderPaging (numProPerPage) {
    // Remove mọi Element có số (Ngoại trừ 2 mũi tên)
    $$("#paging-product li:not(:last-child,:first-child)").forEach(function(e){
        e.remove();
    })

    // Lấy số trang (Số nguyên làm tròn lên khi chia số trang mỗi bảng)
    let numPage = Math.ceil(tempProduct.length/numProPerPage);

    // Xác định vị trí của last child và insert HTML lên trước vị trí đó
    var lastElement = __("#paging-product li:last-child")
    for (let i = 1 ; i <= numPage ; ++i) {
        let renderContent = `<li><a style="cursor:pointer">${i}</a></li>`
        lastElement.insertAdjacentHTML("beforebegin",renderContent);
    }

    // Gán Class active cho vị trí (1), trường hợp không có vị trí 1 - thì không gán
    __("#paging-product li:not(:first-child,:last-child):nth-child(2) a").className="active";
}

// Khi tạo nút số xong thì sẽ add event cho từng nút
function addEventToPaging(numProPerPage) {
    // Mỗi nút sẽ load số sản phẩm tại vị trí tương ứng (Ví dụ 4 SP/trang thì trang 1 từ SP 1-4, trang 2 từ 5-8 ...)
    $$("#paging-product ul li:not(:first-child,:last-child) a").forEach(function(e){
        e.onclick = function(){
            renderProducts(tempProduct.slice(0+((Number(this.innerText)-1)*numProPerPage),numProPerPage+((Number(this.innerText)-1)*numProPerPage)));

            __("#paging-product li a.active").classList.remove("active");

            this.className = "active";

            // Load lại Add to Cart event;
            addEventAddToCart();
        }
    })
    // Kích hoạt render lại trang theo số thứ tự 
    __("#paging-product li a.active").click();
}







